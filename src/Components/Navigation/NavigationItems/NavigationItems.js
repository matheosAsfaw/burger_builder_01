import React from 'react';

import classes from './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = (props) => (
    <ul className={classes.NavigationItems}>
        <NavigationItem link="/"exact >Burger Builder</NavigationItem>
        {props.loggedIn? <NavigationItem link="/orders">Orders</NavigationItem> : null}
        {props.loggedIn ? <NavigationItem link="/logout" >Sign Out</NavigationItem> : <NavigationItem link="/Auth">Sign In</NavigationItem>}
        {/* <NavigationItem link="/checkout">Checkout</NavigationItem> */}
    </ul>
);

export default navigationItems;