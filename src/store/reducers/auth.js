import * as actionsTypes from '../actions/actionsTypes';

const initialState = {
    token: null,
    userId : null,
    error: null,
    loading : false,
    authRedirectPath: '/',
}

const reducer = (state = initialState, actions )=> {
    switch (actions.type) {
        case actionsTypes.SET_AUTH_REDIRECT_PATH:
            return {
                ...state,
                authRedirectPath : actions.path,
            }
        case actionsTypes.AUTH_START:
            return {
                ...state,
                loading : true,
                error: null
            }
        case actionsTypes.AUTH_FAIL: 
            return {
                ...state,
                loading: false,
                error : actions.error,
            }
        case actionsTypes.AUTH_SUCCESS: 
            return {
                ...state,
                loading: false,
                token: actions.idToken,
                userId: actions.userId,
                error:null
            }
        case actionsTypes.AUTH_LOGOUT: 
            return {
                ...state,
                token: null,
                userId: null,
                error:null
            }
        default:
            return state;
    }
}

export default reducer;