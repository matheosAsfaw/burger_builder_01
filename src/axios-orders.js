import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-e2829.firebaseio.com/'
});

export default instance;