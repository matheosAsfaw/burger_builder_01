import React, {Component} from 'react';

import Aux from '../Auxilary/Auxilary';
import Toolbar from "../../Components/Navigation/ToolBar/Toolbar";
import classes from './Layout.css';
import SideDrawer from '../../Components/Navigation/SideDrawer/SideDrawer';
import { connect } from 'react-redux';


class Layout extends Component {
    state ={
        showSideDrawer : false,
    }

    SideDrawerClosedHandler = () => {
        this.setState({showSideDrawer : false});
    }

    SideDrawerToggleHandler =() =>{

        this.setState((prevState)=>{
            return {showSideDrawer : !prevState.showSideDrawer};
        })
    }

    render() {
        return (
            <Aux>
                {/* <div>,SideDrawer,backdrop</div> */}
                <Toolbar drawerToggleClicked={this.SideDrawerToggleHandler} loggedIn={this.props.loggedIn} />
                <SideDrawer open={this.state.showSideDrawer} closed={this.SideDrawerClosedHandler}/>
                <main className={classes.Content} >
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}
const mapStateToprops = state => {
    return {
        loggedIn: state.auth.token? true : false
    }
}

export default connect(mapStateToprops)(Layout);

