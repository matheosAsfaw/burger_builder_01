import React, { Component } from 'react'
import Button from '../../../Components/UI/Button/Button';
import classes from './ContactData.css';
import axios from '../../../axios-orders';
import Spinner from '../../../Components/UI/Spinner/Spinner';
import withErrorHandler from "../../../hoc/WithErrorHandler/withErrorHandler";
import * as actions from '../../../store/actions/index';

import Input from '../../../Components/UI/Input/Input';
import { connect } from 'react-redux'

class ContactData extends Component {

    state ={
        orderForm :{
            name: {
                elementType : 'input',
                elementConfig: {
                    type: 'text',
                    placeholder :'Your Name'
                },
                value :'',
                validation : {
                    required : true,
                },
                valid : false,
                touched : false
            } ,
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            zipCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Zip Code'
                },
                value: '',
                validation: {
                    required: true,
                    minLength:5,
                    maxLength:5
                },
                valid: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Email'
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value:'fastest', displayValue:'Fastest'},
                        {value:'cheapest', displayValue:'Cheapest'}
                    ] 
                },
                value: 'fastest',
                validation: {},
                valid: true
            },
        },
        name :'',
        email:'',
        address: {
            street: '',
            postalCode :''
        },
        formIsValid :false
    }

    orderHandler = (event) => {
        event.preventDefault();

        const formData = {};

        for( let formElementIdentifier in this.state.orderForm){
            formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value
        }

        const order = {
            ingredients: this.props.ings,
            price: this.props.price,
           orderData: formData,
           userId: this.props.userId
        }

        this.props.onOrderBurger(order,this.props.token);
    }

    checkValidity = (value ,rules) => {
        let isValid = true;
        
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        return isValid;
    }

    inputChangedHandler =(event, inputidentifier)=>{
        const updatedOrderform = JSON.parse(JSON.stringify(this.state.orderForm));
        updatedOrderform[inputidentifier].value = event.target.value;
        updatedOrderform[inputidentifier].touched = true;
        updatedOrderform[inputidentifier].valid = this.checkValidity(event.target.value, updatedOrderform[inputidentifier].validation);

        let valid = true;

        for (let key in updatedOrderform){
                valid = updatedOrderform[key].valid && valid;
        }

        this.setState({ orderForm: updatedOrderform, formIsValid: valid });
    }
    
  render() {
    let form = <Spinner/>;
    const formElementsArray = []; 

    for(let key in this.state.orderForm){
        formElementsArray.push({
            id: key,
            config : this.state.orderForm[key]
        });
    }
    if (!this.props.loading){
        form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                   <Input elementType ={formElement.config.elementType}
                     elementConfig = {formElement.config.elementConfig} 
                     value = {formElement.config.value}
                     key = {formElement.id} 
                     invalid = {!formElement.config.valid}
                     shouldValidate={formElement.config.validation}
                     touched ={formElement.config.touched}
                     changed={(event) => this.inputChangedHandler(event, formElement.id)} /> 
                ))}

                <br />
                <Button disabled={!this.state.formIsValid} btnType="Success">ORDER</Button>
            </form>
        )
    };

    return (
        <div className={classes.ContactData}>
            <h4>Enter your Contact Data</h4>
            {form }
        </div>
           
    )
  }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        loading : state.order.loading,
        token : state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        onOrderBurger: (orderData, token) => dispatch(actions.purchaseBurger(orderData, token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData,axios))



